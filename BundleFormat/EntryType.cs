﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BundleFormat
{
    public enum EntryType
    {
        RasterResourceType = 0x00,
        MaterialResourceType = 0x01,
        TextFileResourceType = 0x03,
        RwVertexDescResourceType = 0x0A,
        RwRenderableResourceType = 0x0C,
        unknown_file_type_00D = 0x0D,
        RwTextureStateResourceType = 0x0E,
        MaterialStateResourceType = 0x0F,
        RwShaderProgramBufferResourceType = 0x12,
        RwShaderParameterResourceType = 0x14,
        RwDebugResourceType = 0x16,
        KdTreeResourceType = 0x17,
        SnrResourceType = 0x19,
        AttribSysSchemaResourceType = 0x1B,
        AttribSysVaultResourceType = 0x1C,
        AptDataHeaderType = 0x1E,
        GuiPopupResourceType = 0x1F,
        FontResourceType = 0x21,
        LuaCodeResourceType = 0x22,
        InstanceListResourceType = 0x23,
        IDList = 0x25,
        LanguageResourceType = 0x27,
        SatNavTileResourceType = 0x28,
        SatNavTileDirectoryResourceType = 0x29,
        ModelResourceType = 0x2A,
        RwColourCubeResourceType = 0x2B,
        HudMessageResourceType = 0x2C,
        HudMessageListResourceType = 0x2D,
        unknown_file_type_02E = 0x2E,
        unknown_file_type_02F = 0x2F,
        WorldPainter2DResourceType = 0x30,
        PFXHookBundleResourceType = 0x31,
        ShaderResourceType = 0x32,
        ICETakeDictionaryResourceType = 0x41,
        VideoDataResourceType = 0x42,
        PolygonSoupListResourceType = 0x43,
        CommsToolListDefinitionResourceType = 0x45,
        CommsToolListResourceType = 0x46,
        AnimationCollectionResourceType = 0x51,
        RegistryResourceType = 0xA000,
        GenericRwacWaveContentResourceType = 0xA020,
        GinsuWaveContentResourceType = 0xA021,
        AemsBankResourceType = 0xA022,
        CsisResourceType = 0xA023,
        NicotineResourceType = 0xA024,
        SplicerResourceType = 0xA025,
        GenericRwacReverbIRContentResourceType = 0xA028,
        SnapshotDataResourceType = 0xA029,
        ZoneListResourceType = 0xB000,
        LoopModelResourceType = 0x10000,
        AISectionsResourceType = 0x10001,
        TrafficDataResourceType = 0x10002,
        TriggerResourceType = 0x10003,
        VehicleListResourceType = 0x10005,
        GraphicsSpecResourceType = 0x10006,
        ParticleDescriptionCollectionResourceType = 0x10008,
        WheelListResourceType = 0x10009,
        WheelGraphicsSpecResourceType = 0x1000A,
        TextureNameMapResourceType = 0x1000B,
        ProgressionResourceType = 0x1000E,
        PropPhysicsResourceType = 0x1000F,
        PropGraphicsListResourceType = 0x10010,
        PropInstanceDataResourceType = 0x10011,
        BrnEnvironmentKeyframeResourceType = 0x10012,
        BrnEnvironmentTimeLineResourceType = 0x10013,
        BrnEnvironmentDictionaryResourceType = 0x10014,
        GraphicsStubResourceType = 0x10015,
        StaticSoundMapResourceType = 0x10016,
        StreetDataResourceType = 0x10018,
        BrnVFXMeshCollectionResourceType = 0x10019,
        MassiveLookupTableResourceType = 0x1001A,
        VFXPropCollectionResourceType = 0x1001B,
        StreamedDeformationSpecResourceType = 0x1001C,
        ParticleDescriptionResourceType = 0x1001D,
        PlayerCarColoursResourceType = 0x1001E,
        ChallengeListResourceType = 0x1001F,
        FlaptFileResourceType = 0x10020,
        ProfileUpgradeResourceType = 0x10021,
        VehicleAnimationResourceType = 0x10023,
        BodypartRemappingResourceType = 0x10024,
        LUAListResourceType = 0x10025,
        LUAScriptResourceType = 0x10026,

        Invalid = 0x99999
    }
}
